function mutliply(n1, n2){
    let result = 0;
    for (let i = 0; i < n2; i++) {
        result += n1;
    }
    return result;
}

let finalResult = mutliply(4, 5);
console.log(finalResult);

function divide(n1, n2){
    let result = 1;
    while(n1 - n2 > 0){
        result++;
        n1 -= n2;
    }
    return result;
}
finalResult = divide(20,4);

console.log(finalResult)

finalResult = divide(mutliply(5, 4), 10);

console.log(finalResult);

finalResult = mutliply(divide(mutliply(5, 4), 10), 6);

console.log(finalResult);

function mutliplyCallBack(success, error){
    try{
        let n1 = arguments[2];
        let n2 = arguments[3];

        if(n1 == null || n2 == null){
            throw("Missing arguments: expected 2 numbers.");
        }
        let result = mutliply(n1, n2); 
        success(result);
    }catch(e){
        error(e);
    }
}

function okCallBack(result){
    console.log(result);
}

function errorCallBack(error){
    console.log(error);
}

console.log("-----CallBacks------")

mutliplyCallBack(okCallBack, errorCallBack, 5, 2);

mutliplyCallBack((result) => {
    mutliplyCallBack(okCallBack, errorCallBack, result, 4);
}, errorCallBack, 5, 2);

function divideCallback(success, error){
    try{
        let n1 = arguments[2];
        let n2 = arguments[3];

        if(n1 == null || n2 == null){
            throw("Missing arguments: expected 2 numbers.");
        }
        let result = divide(n1, n2); 
        success(result);
    }catch(e){
        error(e);
    }
}

divideCallback(okCallBack, errorCallBack, 20, 4);

mutliplyCallBack((result) => {
    divideCallback((result) => {
        mutliplyCallBack(okCallBack, errorCallBack, 5, result);
    }, errorCallBack, result, 10);
}, errorCallBack, 5, 4)

// ES6 => new Primise()

function mutliplyPriomise(result){
    return new Promise((resolve, reject) => {
        if(result.n1 == null || result.n2 == null){
            throw("Missing arguments: expected 2 numbers.");
        }else {
            let r = new Object();
            r.n1 = mutliply(result.n1, result.n2);
            resolve(r);
        }
    });
}

let numbers = new Object();
numbers.n1 = 5;
numbers.n2 = 4;
mutliplyPriomise(numbers)
.then(okCallBack)
.catch(errorCallBack);

function dividePromise(result){
    return new Promise((resolve, reject) => {
        if(result.n1 == null || result.n2 == null){
            throw("Missing arguments: expected 2 numbers.");
        }else if(result.n2 != 0){
            let r = new Object
            r.n1 = divide(result.n1, result.n2);
            resolve(r);
        }else{
            throw("Divide by cero");
        }
    });
}

numbers.n1 = 20;
numbers.n2 = 4;

dividePromise(numbers)
.then(okCallBack)
.catch(errorCallBack);


numbers.n1 = 20;
numbers.n2 = 4;

mutliplyPriomise(numbers)
.then((data) => {
    data.n2 = 10;
    return dividePromise(data)
})
.then((data) => {
    data.n2 = 5;
    return mutliplyPriomise(data);
})
.then(okCallBack)
.catch(errorCallBack);

async function operations(){
    try{
        let num = new Object();
        num.n1 = 5;
        num.n2 = 4; 

        let val1 = await mutliplyPriomise(num);
        num.n1 = val1.n1;
        num.n2 = 10;

        let val2 = await dividePromise(num);
        num.n1 = val2.n1;
        num.n2 = 5;
        let val3 = await mutliplyPriomise(num);
        okCallBack(val3.n1);
    }catch(e){
        errorCallBack(e);
    }
}

operations();