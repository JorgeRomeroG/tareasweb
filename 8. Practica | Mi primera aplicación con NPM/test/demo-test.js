const assert = require('assert');
const sumar = require('../app')

describe('Probar las sumas de numeros', ()=>{
    it('Cinco mas Cinco es diez', ()=>{
        assert.equal(10, sumar(5,5));
    });
    it('Cinco mas cinco no es veinte', ()=>{
        assert.notEqual(20, sumar(5,5));
    })
});