/*

2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo:
> use students
switched to db students
> db.grades.count()
800 

3) Encuentra todas las calificaciones del estudiante con el id numero 4

> db.grades.find({student_id: 4}, {score:1})
{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "score" : 87.89071881934647 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "score" : 27.29006335059361 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "score" : 28.656451042441 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "score" : 5.244452510818443 }

4) ¿Cuántos registros hay de tipo exam?

> db.grades.count({type: "exam"})
200

5) ¿Cuántos registros hay de tipo homework?
> db.grades.count({type: "homework"})
400

6) ¿Cuántos registros hay de tipo quiz?
> db.grades.count({type: "quiz"})
200

7) Elimina todas las calificaciones del estudiante con el id numero 3
> db.grades.remove({student_id:3})
WriteResult({ "nRemoved" : 4 })

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
> db.grades.find({type: "homework", score:75.29561445722392}, {student_id:1, _id: 0})
{ "student_id" : 9 }

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
> db.grades.update({_id :ObjectId("50906d7fa3c412bb040eb591")}, {score:100})
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

10) A qué estudiante pertenece esta calificación.
> db.grades.find({_id: ObjectId("50906d7fa3c412bb040eb591")})
{ "_id" : ObjectId("50906d7fa3c412bb040eb591"), "score" : 100 }


*/