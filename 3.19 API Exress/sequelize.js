const Sequelize = require('sequelize');
const log4js = require('log4js');
const logger = log4js.getLogger();

const actorModel = require('./models/actor');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const directorModel = require('./models/director');
const copyModel = require('./models/copy');
const bookingModel = require('./models/booking');
const memberModel = require('./models/member');
const moviesActorsModel = require('./models/moviesactors');

logger.level = 'debug';


const sequelize = new Sequelize('video-club', 'root','secret', {
  host:'localhost',
  dialect:'mysql'
});

const Actor = actorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const MoviesActors = moviesActorsModel(sequelize, Sequelize);
 

Genre.hasMany(Movie, {as: 'movies'});
Movie.belongsTo(Genre, {as: 'genre'});

Director.hasMany(Movie, {as: 'movies'});
Movie.belongsTo(Director, {as:'director'});


Movie.hasMany(Copy, {as:'copies'});
Copy.belongsTo(Movie, {as:'movie'});

Copy.hasMany(Booking, {as:'bookings'});
Booking.belongsTo(Copy, {as:'copy'});

Member.hasMany(Booking, {as:'bookings'});
Booking.belongsTo(Member, {as:'member'});


MoviesActors.belongsTo(Movie, {foreignKey: 'movieId'});
MoviesActors.belongsTo(Actor, {foreignKey: 'actorId'});

Movie.belongsToMany(Actor, {
  through: 'moviesActors',
  foreignKey: 'actorId',
  as: 'actors'
});

Actor.belongsToMany(Movie, {
  through: 'moviesActors',
  foreignKey: 'movieId',
  as: 'movies'
});


sequelize.sync({
  force: true
}).then(()=>{
  logger.info("Database created !!!!");
});

module.exports = {
  Actor, Genre, Movie
};
