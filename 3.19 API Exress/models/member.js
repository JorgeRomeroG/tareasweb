module.exports = (sequelize, type) => {
    const Member = sequelize.define('member', {
      id: {type: type.INTEGER, primaryKey:true, autoIncrement: true},
      name: type.STRING,
      last_name: type.STRING,
      address: type.STRING,
      phone: type.STRING,
      status: type.BOOLEAN
    });
  
    return Genre;
  };
  