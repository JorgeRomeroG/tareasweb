const json = require('./product.json');
class DAO{
    getJson(){
        return json;
    }
    greater20(success, error){
        try {
            let fl = arguments[2];
            let num = 0;
            if(fl == null){
                throw('Missing Arguments');
            }
            for (let i = 0; i < fl.length; i++) {
                if(fl[i].price > 20){
                    num++;
                }
            }
            success(num);
        } catch (e) {
            error(e);
        }
    }
    lower15(success, error){
        try {
            let fl = arguments[2];
            let num = 0;
            if(fl == null){
                throw('Missing Arguments');
            }
            for (let i = 0; i < fl.length; i++) {
                if(fl[i].price < 15){
                    num++;
                }
            }
            success(num);
        } catch (e) {
            error(e);
        }
    }
    greater15Category(success, error){
        try {
            let fl = arguments[2];
            let cat = [];
            if(fl == null){
                throw('Missing Arguments');
            }
            for (let i = 0; i < fl.length; i++) {
                if(fl[i].price > 15.50){
                    if(cat[fl[i].category] == null){
                        cat[fl[i].category] = 1;
                    }else {
                        cat[fl[i].category]++;
                    }
                }
            }
            success(cat);
        } catch (e) {
            error(e);
        }
    }
    greater20Lower45(success, error){
        try {
            let fl = arguments[2];
            let cat = [];
            if(fl == null){
                throw('Missing Arguments');
            }
            for (let i = 0; i < fl.length; i++) {
                if(fl[i].price > 20.30 && fl[i].price < 45){
                    if(cat[fl[i].category] == null){
                        cat[fl[i].category] = 1;
                    }else {
                        cat[fl[i].category]++;
                    }
                }
            }
            success(cat);
        } catch (e) {
            error(e);
        }
    }
    categories(success, error){
        try {
            let fl = arguments[2];
            let cat = [];
            if(fl == null){
                throw('Missing Arguments');
            }
            for (let i = 0; i < fl.length; i++) {
                if(cat[fl[i].category] == null){
                    cat[fl[i].category] = 1;
                }else {
                    cat[fl[i].category]++;
                }
            }
            success(cat);
        } catch (e) {
            error(e);
        }
    }

}
module.exports = DAO;