const DAO = require('./dao.js');
let d = new DAO();
let products = d.getJson();

function error(err){
    console.log(err);
    
}

d.greater20((res) =>{
    console.log(`Productos mayor a 20: ${res}`);
}, error, products);

d.lower15((res)=>{
    console.log(`Productos menor a 15: ${res}`);
}, error, products);

d.greater15Category((res) => {
    console.log(`Categorias con productos mayor a 15.5:\n`, res);
}, error, products);

d.greater20Lower45((res) => {
    console.log(`Categorias con productos mayor a 20.30 y menor a 45:\n`, res);
}, error, products);

d.categories((res) => {
    console.log(`Categorias con productos:\n`, res);
}, error, products);