let express = require('express');
let router = express.Router();
const resultsController = require('../controllers/results')


router.get('/results/:n1/:n2', resultsController.sum);
router.post('/results/:n1/:n2', resultsController.multiply);
router.put('/results/:n1/:n2', resultsController.divide);
router.delete('/results/:n1/:n2', resultsController.sub);



module.exports = router;
