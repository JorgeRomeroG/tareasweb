const express = require('express');

function sum(req, res, next) {
    let n1 = parseInt(req.params.n1);
    let n2 = parseInt(req.params.n2);
    res.json(n1+n2);
  }

function multiply(req, res, next) {
    let n1 = parseInt(req.params.n1);
    let n2 = parseInt(req.params.n2);
    res.json(n1*n2);
}

function divide(req, res, next) {
    let n1 = parseInt(req.params.n1);
    let n2 = parseInt(req.params.n2);
    res.json(n1/n2);
}

function sub(req, res, next) {
    let n1 = parseInt(req.params.n1);
    let n2 = parseInt(req.params.n2);
    res.json(n1-n2);
}
module.exports = {
    sum, multiply, divide, sub
}