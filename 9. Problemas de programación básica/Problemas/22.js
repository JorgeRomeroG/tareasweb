var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Introduzca palabra: \n");

function getTabs(num){
    let s = '';
    for (let i = 0; i < num; i++) {
        s += '\t';
    }
    return s;
}


standard_input.on('data', function (w) {
   
    for (let i = 0; i < 5; i++) {
       console.log(getTabs(i) + w);
        
    }
        
    process.exit();
   
    
});
