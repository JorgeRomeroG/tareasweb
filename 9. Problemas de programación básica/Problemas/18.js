var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Inserte Palabra seguida por letra: \n");

let s;
let c;
standard_input.on('data', function (string) {
   if(!s){
       s = string;
   }else{
       c=string;
       let letter_Count = 0;
        for (let position = 0; position < s.length; position++) 
        {
            if (s.charAt(position) === c.charAt(0)) 
            {
                letter_Count += 1;
            }
        }
        console.log("Cantidad de " + c + ": " + letter_Count);
        
       process.exit();
   }
    
});
