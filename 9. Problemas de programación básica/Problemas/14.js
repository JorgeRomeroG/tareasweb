var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Inserte 5 numero: \n");

let num = 0;
let max;
let min;
standard_input.on('data', function (data) {
    if(num == 0){
        max = data;
        min = data;
    }
    max = Math.max(max, data);
    min = Math.min(min, data);
    if(num == 4){
        console.log("Numero max: " + max);      
        console.log("Numero min: " + min);        
        process.exit();
    }else num++;
});
