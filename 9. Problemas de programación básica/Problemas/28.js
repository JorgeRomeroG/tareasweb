function coin(){
    return Math.floor(Math.random() * 2);     // returns a random integer from 0 to 1
}

if(coin()){
    console.log("Heads");
}else {
    console.log("Tails");
}