
function medi(list){
    let sum = 0;
    for (let i = 0; i < list.length; i++) {
        sum += parseInt(list[i],10);
    }
    return sum/list.length;
}

var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Introduzca 10 Calificaciones: \n");

let calif = [];
let count = 0;
standard_input.on('data', function (va) {
    if(count != 10){
        calif.push(va.substring(0, va.length-1));
        count++;
    }
    if(count == 10){
        let v = medi(calif) ;       
        console.log('El promedio es:' + v);
        process.exit();
    }
});

