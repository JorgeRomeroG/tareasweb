var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Inserte 2 numero: \n");

let num1;
let num2;
standard_input.on('data', function (data) {
    if(!num1){
        num1 = data;
    }else if(!num2){
        num2 = data;
    

        let min = Math.min(num1, num2);
        let max = Math.max(num1, num2);
        console.log("Numero naturales: " );

        let cant = 0;
        let cantpar = 0;
        let impsum = 0;

        for (let i = min; i <= max; i++) {
            console.log(i);
            if(i %2 == 0){
                cantpar++;
                impsum += i;
            }

            cant++;
        }

        console.log("Canitidad de naturales: " + cant);
        console.log("Canitidad de pares: " + cantpar);
        console.log("Suma de Pares: " + impsum);
        process.exit();
    }
});
