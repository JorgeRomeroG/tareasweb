

var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Introduzca un numero para convertir a romano: \n");

function toRoman (num) {
    if (isNaN(num))
        return NaN;
    var d = String(+num).split(""),
        key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
               "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
               "","I","II","III","IV","V","VI","VII","VIII","IX"],
        roman = "",
        i = 3;
    while (i--)
        roman = (key[+d.pop() + (i * 10)] || "") + roman;
    return Array(+d.join("") + 1).join("M") + roman;
}


standard_input.on('data', function (string) {
    console.log(toRoman(string));
    process.exit();
});
