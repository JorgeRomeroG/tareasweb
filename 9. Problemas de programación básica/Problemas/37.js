let calif = [];
let readline = require('readline-sync');

let courses = readline.question("How many courses? ");
let students;

for (let i = 0; i < courses; i++) {
    calif[i] = [];
    students = readline.questionInt("How many students in course " + (i+1) + "? ");
    for (let j = 0; j < students; j++) {
        let cal = readline.questionFloat("Calification student " + (j+1) +  "? ");
        calif[i][j] = cal;
    }    
}

for (let i = 0; i < courses; i++) {
    for (let j = 0; j < students; j++) {

        console.log('Calificaciones curso ' + (i+1) + ' Estudiante '+ (j+1) + ': ' +  calif[i][j]);
    }    
}
