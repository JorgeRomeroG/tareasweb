

var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Introduzca un numero: \n");

let n1;
standard_input.on('data', function (va) {
    if(!n1){
        n1 = va;
        console.log("1. Consultar es primo \n2. Factorial \n3. Tabla de multiplicar");
    }else{
        if(va == 1){
            console.log('Es Primo:', isPrime(n1));
        }
        if(va == 2){
            console.log(fact(n1));
        }
        if(va == 3){
            for (let i = 1; i <= 10; i++) {
                console.log(n1.substring(0,n1.length-1) + ' x ' + i + ' = ' + n1* i);
            }        }
        process.exit();
    }
    
});

function isPrime(num){
    for (let i = num -1; i > 1 ; i--) {
        if(num % i == 0) return false;        
    }
    return true;
}
function fact(num){
    if(num>1)
        return num*fact(num-1);
    else return 1;
}