function dice(){
    return Math.floor(Math.random() * 6) +1;     // returns a random integer from 0 to 1
}
let n = 0;

for (let i = 0; i < 100; i++) {
    if(dice() + dice() == 10){
        n++;
    }
}

console.log("Cantidad de que los dados sumen 10: " + n);