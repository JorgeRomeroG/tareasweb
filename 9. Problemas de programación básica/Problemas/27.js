var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Introduzca numero de tabla de mult del 0 al 10: \n");

standard_input.on('data', function (data) {
    
    for (let i = 1; i <= 10; i++) {
        console.log(data.substring(0,data.length-1) + ' x ' + i + ' = ' + data* i);
    }
    process.exit();
});
