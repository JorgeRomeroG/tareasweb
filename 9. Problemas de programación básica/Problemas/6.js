const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Hasta qué número quieres mostrar? ', (answer) => {
  // TODO: Log the answer in a database
  for (let i = 1; i <= answer; i++) {
    console.log(i);    
}

  rl.close();
});