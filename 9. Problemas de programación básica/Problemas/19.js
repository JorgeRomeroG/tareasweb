var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Introduzca hora seguida por hora: \n");

let h = 0.0;
let m = 0.0;
standard_input.on('data', function (string) {
   if(!h){
       if(string >= 0 || string < 24){
           h = string;
       }
   }else if (string >= 0 || string <= 59){
       m=string;
        console.log("Son las: " + h.substring(0,2) + ":" + m);
        
       process.exit();
   }
    
});
