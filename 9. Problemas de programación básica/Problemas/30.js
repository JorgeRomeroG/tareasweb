

var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Introduzca dos numeros: \n");

let n1;
let n2;
standard_input.on('data', function (va) {
    if(!n1){
        n1 = va;
    }else if(!n2){
        n2 = va;
        console.log("1. Multiplicar \n2.Sumar \n3.Restar \n4.Dividir");
    }else{
        if(va == 1){
            console.log(n1*n2);
        }
        if(va == 2){
            console.log(n1+n2);
        }
        if(va == 3){
            console.log(n1-n2);
        }
        if(va == 4){
            console.log(n1/n2);
        }
        process.exit();
    }
    
});
