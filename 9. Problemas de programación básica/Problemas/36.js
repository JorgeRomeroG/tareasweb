let m = [];

console.log('Matriz normal.');

for (let i  = 0; i < 4; i++) {
    m[i] = [];
    for (let j = 0; j < 5; j++) {
        let v = Math.floor(Math.random() * 100) + 1;
        m[i][j] = v ;
        process.stdout.write(v.toString() + " ");    
    }
    console.log();
    
}
console.log();

console.log('Matriz Transpuesta.');

for (let i  = 0; i < 5; i++) {
    for (let j = 0; j < 4; j++) {
        let v =  m[j][i];
        process.stdout.write(v.toString() + " ");    
    }
    console.log();
    
}
