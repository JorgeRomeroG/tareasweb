var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Salir? (S/N)\n");


standard_input.on('data', function (data) {

    if(data === 'S\n' || data === 's\n'){
        console.log("Saliendo.");
        process.exit();
    }else if(data === 'N\n' || data === 'n\n')
    {
        // Print user input in console.
        console.log('Salir? (S/N)');
    }
    else{
        console.log("Error: Entrada no valida.")
    }
});