var standard_input = process.stdin;

standard_input.setEncoding('utf-8');

console.log("Inserte numero: \n");

let n = 0;

standard_input.on('data', function (data) {

    for (let index = 1; index <= data; index++) {        
        if(index % 3 == 0){
            n++;
        }
    }

    console.log("Numeros multiplos de 3: " + n);
   
    process.exit();
});